<?php

namespace Venturo\CrudGenerator\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use ReflectionClass;
use ReflectionException;

class CrudGeneratorCommand extends Command
{
    protected $signature = 'make:crud {name : The name of the model} {--fields= : The fields for the migration} {--attributes= : The attributes column for the migration} {--filters= : The filters for the model} {--create-rule= : The create rules for the request} {--update-rule= : The update rules for the request} {--relations= : The relations for the model} {--files= : The files are created} {--include= : The include for the parent table} {--table= : The table name for the migration}';
    protected $description = 'Generate CRUD operations';

    public function handle(): void
    {
        $name = $this->argument('name');
        $fields = $this->option('fields');
        $attributes = $this->option('attributes');
        $filters = $this->option('filters');
        $createRule = $this->option('create-rule');
        $updateRule = $this->option('update-rule');
        $relations = $this->option('relations');
        $files = $this->option('files');
        $table = $this->option('table');
        $include = $this->option('include');
        if (empty($files) || $files == "") {
            $files = "migration,model,helper,controller,request,resource,routes";
        }
        if (str_contains(strtolower($files), "migration") && (empty($table) || $table == "")) {
            $this->createMigration($name, $fields, $attributes, $relations);
        }
        if (str_contains(strtolower($files), "model")) {
            $this->createModel($name, $fields, $filters, $relations, $table);
        }
        if (str_contains(strtolower($files), "helper")) {
            if (empty($include) && $include === "") {
                $this->createHelper($name, $createRule, $updateRule);
            } else {
                $this->editHelper($name, $include, $relations);
            }
        }
        if (str_contains(strtolower($files), "controller")) {
            if (empty($include) && $include === "") {
                $this->createController($name, $fields, $filters);
            } else {
                $this->editController($name, $include, $filters);
            }
        }
        if (str_contains(strtolower($files), "request")) {
            if (empty($include) && $include === "") {
                $this->createRequest($name, $createRule, $updateRule);
            } else {
                $this->editRequest($name, $include, $createRule, $updateRule);
            }
        }
        if (str_contains(strtolower($files), "resource")) {
            $this->createResource($name);
        }
        if ((empty($include) && $include === "") && (str_contains(strtolower($files), "routes"))) {
            $this->appendRoutes($name);
        }
        if (!empty($table) && $table !== "") {
            $scriptPath = realpath(__DIR__);
            for ($i = 0; $i < 5; $i++) {
                $scriptPath = dirname($scriptPath);
            }
            chdir($scriptPath);
            $output = shell_exec('php artisan migrate:generate ' .
                '--tables=' . escapeshellarg(trim($table)) . ' ' .
                '--skip-log ' .
                '--skip-vendor ' .
                '--skip-views ' .
                '--skip-proc ' .
                '2>&1');
        }

        $this->info('CRUD for ' . $name . ' created successfully.');
    }

    protected function createMigration($name, $fields, $attributes, $relations): void
    {
        $tableName = $this->prepareTableName($name, $relations);

        $migrationTemplate = str_replace(
            ['{{tableName}}', '{{columns}}'],
            [$tableName, $this->prepareColumns($fields, $attributes)],
            $this->getStub('migration')
        );

        $migrationFileName = date('Y_m_d_His') . '_create_' . $tableName . '_table.php';
        file_put_contents(database_path("/migrations/$migrationFileName"), $migrationTemplate);
    }

    protected function createModel($name, $fields, $filters, $relations, $table): void
    {
        $relations = $this->prepareRelations($name, $relations);
        $capitalizedName = ucfirst($name);

        $modelTemplate = str_replace(
            ['{{modelName}}', '{{fillable}}', '{{tableName}}', '{{queryFilters}}', '{{relations}}'],
            [$capitalizedName, $this->prepareFillable($fields), $this->prepareTableName($name, $relations, $table), $this->prepareQueryFilters($filters), $relations],
            $this->getStub('model')
        );

        file_put_contents(app_path("/Models/{$capitalizedName}Model.php"), $modelTemplate);
    }

    protected function createHelper($name, $createRule, $updateRule): void
    {
        $capitalizedName = ucfirst($name);
        $varName = lcfirst(Str::camel($name));
        $createPayload = $this->prepareUploadPayload($createRule);
        $updatePayload = $this->prepareUploadPayload($updateRule);
        $uploadPayload = [];

        if ($createPayload['uploadFunction'] !== '') {
            $uploadPayload['uploadFunction'] = $createPayload['uploadFunction'];
            $uploadPayload['constUpload'] = $createPayload['constUpload'];
        } elseif ($updatePayload['uploadFunction'] !== '') {
            $uploadPayload['uploadFunction'] = $updatePayload['uploadFunction'];
            $uploadPayload['constUpload'] = $updatePayload['constUpload'];
        } else [
            $uploadPayload['uploadFunction'] = '',
            $uploadPayload['constUpload'] = ''
        ];

        $helperTemplate = str_replace(
            ['{{modelName}}', '{{varName}}', '{{uploadFunction}}', '{{constUpload}}', '{{initiateUploadCreate}}', '{{initiateUploadUpdate}}'],
            [$capitalizedName, $varName, $uploadPayload['uploadFunction'], $uploadPayload['constUpload'], $createPayload['initiateUpload'], $updatePayload['initiateUpload']],
            $this->getStub('helper')
        );

        $directory = app_path("Helpers/$capitalizedName");

        if (!File::isDirectory($directory)) {
            File::makeDirectory($directory);
        }

        file_put_contents("$directory/{$capitalizedName}Helper.php", $helperTemplate);
    }

    protected function editHelper($name, $parentModel, $relations): void
    {
        $capitalizedName = ucfirst($name);
        $name = lcfirst(Str::camel($name));
        $parentName = str_replace('Model', '', $parentModel);
        $helperPath = app_path("Helpers/$parentName/{$parentName}Helper.php");
        $helperContent = file_get_contents($helperPath);

        $importModel = $this->prepareImportModelChildren($name, $helperContent, $parentModel);
        $initiateVar = $this->prepareVarChildren($name, $helperContent, $capitalizedName);
        $callMethod = $this->callMethodChildren($name, $helperContent);

        $helperTemplate = $this->addMethodCrudChildren($name, $helperContent, $parentModel, $relations);
        $helperTemplate = str_replace(
            [$importModel['originalImport'], $initiateVar['originalVar'], $initiateVar['originalInitiate'], $callMethod['originalCreate'], $callMethod['originalUpdate']],
            [$importModel['importModel'], $initiateVar['addVar'], $initiateVar['addInitiate'], $callMethod['addCallMethodCreate'], $callMethod['addCallMethodUpdate']],
            $helperTemplate
        );

        file_put_contents($helperPath, $helperTemplate);
    }

    protected function createController($name, $fields, $filters): void
    {
        $modelName = ucfirst($name);
        $modelVariable = lcfirst($name);
        $lowerName = strtolower($name);
        $payload = $this->prepareFillable($fields);

        $controllerTemplate = str_replace(
            ['{{modelName}}', '{{modelVariable}}', '{{lowerName}}', '{{payload}}', '{{filters}}'],
            [$modelName, $modelVariable, $lowerName, $payload, $this->prepareFilters($filters)],
            $this->getStub('controller')
        );

        file_put_contents(app_path("/Http/Controllers/Api/{$modelName}Controller.php"), $controllerTemplate);
    }

    protected function editController($name, $parent, $filters): void
    {
        $parentController = str_replace('Model', 'Controller', $parent);

        $controllerPath = app_path("Http/Controllers/Api/$parentController.php");
        $controllerContent = file_get_contents($controllerPath);

//        $filters = $this->prepareChangeFilters($controllerContent, $filters);
        $payloadCreate = $this->prepareChangePayload($controllerContent, $name);
        $payloadUpdate = $this->prepareChangePayload($controllerContent, $name, true);

//        $controllerContent = str_replace($filters['original'], $filters['new'], $controllerContent);
        $posCreate = strpos($controllerContent, $payloadCreate['original']);
        if ($posCreate !== false) {
            $controllerContent = substr_replace($controllerContent, $payloadCreate['new'], $posCreate, strlen($payloadCreate['original']));
        }

        $posUpdate = strrpos($controllerContent, $payloadUpdate['original']);
        if ($posUpdate !== false) {
            $controllerContent = substr_replace($controllerContent, $payloadUpdate['new'], $posUpdate, strlen($payloadUpdate['original']));
        }
        file_put_contents($controllerPath, $controllerContent);
    }

    protected function createRequest($modelName, $createRules, $updateRules): void
    {
        $createValidationRules = $this->prepareValidationRules($createRules);
        $updateValidationRules = $this->prepareValidationRules($updateRules);
        $checkBase64 = $this->prepareBase64($createRules, $updateRules);
        $formattedModelName = ucfirst($modelName);

        $requestTemplate = str_replace(
            ['{{modelName}}', '{{createRules}}', '{{updateRules}}', '{{base64}}', '{{base64Import}}', '{{useBase64}}'],
            [$formattedModelName, $createValidationRules, $updateValidationRules, $checkBase64['base64'], $checkBase64['base64Import'], $checkBase64['useBase64']],
            $this->getStub('request')
        );

        $requestDirectoryPath = app_path("Http/Requests");
        if (!File::exists($requestDirectoryPath)) {
            File::makeDirectory($requestDirectoryPath);
        }

        file_put_contents("$requestDirectoryPath/{$formattedModelName}Request.php", $requestTemplate);
    }

    protected function editRequest($name, $parent, $createRules, $updateRules): void
    {
        $name = lcfirst(Str::camel(Str::plural($name)));
        $parentRequest = str_replace('Model', 'Request', $parent);

        $requestPath = app_path("Http/Requests/$parentRequest.php");
        $requestContent = file_get_contents($requestPath);

        $createValidationRules = $this->prepareChangeValidationRules($name, $requestContent, $createRules);
        $updateValidationRules = $this->prepareChangeValidationRules($name, $requestContent, $updateRules, true);

        $requestContent = str_replace(
            [$createValidationRules['original'], $updateValidationRules['original']],
            [$createValidationRules['new'], $updateValidationRules['new']],
            $requestContent
        );

        file_put_contents($requestPath, $requestContent);
    }

    protected function createResource($name): void
    {
        $name = ucfirst($name);
        $resource = $this->prepareAttributes($name);

        $resourceTemplate = str_replace(
            ['{{modelName}}', '{{attributes}}', '{{resourceImports}}'],
            [$name, $resource['attributes'], $resource['resourceImports']],
            $this->getStub('resource')
        );

        $resourceDirectory = app_path("/Http/Resources");
        if (!File::exists($resourceDirectory)) {
            File::makeDirectory($resourceDirectory);
        }

        file_put_contents("$resourceDirectory/{$name}Resource.php", $resourceTemplate);
    }

    protected function prepareTableName($name, $relations, $table = null): string
    {
        if (!empty($table) && $table !== '') {
            return $table;
        }

        if ($relations) {
            $prefix = 't_';
        } else {
            $prefix = 'm_';
        }

        return $prefix . Str::snake(Str::plural($name));
    }

    protected function prepareColumns($fields, $attributes): string
    {
        if (!$fields) {
            return '';
        }

        $columns = '';
        $fieldsArray = explode(';', $fields);
        $attributesArray = [];

        if ($attributes) {
            $attributesArray = explode(';', $attributes);
            $attributesArray = array_map(function ($attr) {
                $parts = explode('=', $attr);
                return [trim($parts[0]) => isset($parts[1]) ? trim($parts[1]) : null];
            }, $attributesArray);
            $attributesArray = array_merge(...$attributesArray);
        }

        foreach ($fieldsArray as $field) {
            list($name, $type) = explode('=', $field);

            if (str_contains($type, ',')) {
                if (str_contains($type, 'enum') || str_contains($type, 'set')) {
                    list($type, $constraint) = explode(':', $type);
                    $enumValues = explode(',', $constraint);
                    $enumValuesString = implode("','", $enumValues);
                    $column = "\$table->$type('$name', ['$enumValuesString'])";
                } else {
                    list($type, $constraint) = explode(':', $type);
                    $column = "\$table->$type('$name', $constraint)";
                }
            } elseif (str_contains($type, ':')) {
                list($type, $constraint) = explode(':', $type, 2);
                $column = "\$table->$type('$name', $constraint)";
            } else {
                $column = "\$table->$type('$name')";
            }

            if (isset($attributesArray[$name])) {
                $additionalAttributes = explode(',', $attributesArray[$name]);
                foreach ($additionalAttributes as $attribute) {
                    if ($attribute === 'nullable') {
                        $column .= "->nullable()";
                    } elseif (str_starts_with($attribute, 'default:')) {
                        $defaultValue = str_replace('default:', '', $attribute);
                        $column .= "->default('$defaultValue')";
                    }
                }
            }

            $column .= ";";
            $columns .= $column . "\n\t\t\t";
        }

        return rtrim($columns);
    }

    protected function prepareRelations($name, $relations): string
    {
        if (!$relations) {
            return '';
        }

        $relationMethods = '';
        $relations = explode(';', $relations);

        foreach ($relations as $relation) {
            $relationParts = explode(',', $relation);

            list($relationType, $relationParent, $relationModel, $relationForeignKey, $relationLocalKey) = $relationParts;

            $relationName = lcfirst(str_replace('Model', '', $relationModel));
            $relationMethods .= "\n\tpublic function $relationName()\n\t{";
            $relationMethods .= "\n\t\treturn \$this->$relationType($relationModel::class, '$relationForeignKey', '$relationLocalKey');";
            $relationMethods .= "\n\t}\n";

            $filePath = app_path('Models/' . $relationModel . '.php');

            if (file_exists($filePath)) {
                $relatedModelPath = $filePath;
                $relatedModelContent = file_get_contents($relatedModelPath);
                $relationParentName = Str::plural(lcfirst($name));
                $relationParentClassName = ucfirst($name) . 'Model';
                $relationParentMethod = "\n\tpublic function $relationParentName()\n\t{";
                $relationParentMethod .= "\n\t\treturn \$this->$relationParent($relationParentClassName::class, '$relationForeignKey', '$relationLocalKey');";
                $relationParentMethod .= "\n\t}";

                if (!str_contains($relatedModelContent, "function $relationParentName")) {
                    $newModelContent = preg_replace('/}\s*$/', "$relationParentMethod\n}", $relatedModelContent);
                    file_put_contents($relatedModelPath, $newModelContent);
                }
            }
        }

        return $relationMethods;
    }

    protected function prepareQueryFilters($filters): string
    {
        if (!$filters) {
            return '';
        }

        $queryFilter = '';
        $filters = explode(';', $filters);

        foreach ($filters as $value) {
            $filterParts = explode('=', $value);
            $queryFilter .= "\n\t\tif (!empty(\$filter['" . $filterParts[0] . "'])) {\n";

            $queryFilter .= match ($filterParts[1]) {
                'like' => "\t\t\t\$query->where('" . $filterParts[0] . "', 'LIKE', '%' . \$filter['" . $filterParts[0] . "'] . '%');",
                'whereNot' => "\t\t\t\$query->where('" . $filterParts[0] . "', '!=', \$filter['" . $filterParts[0] . "']);",
                'whereIn' => "\t\t\t\$query->whereIn('" . $filterParts[0] . "', \$filter['" . $filterParts[0] . "']);",
                'whereNotIn' => "\t\t\t\$query->whereNotIn('" . $filterParts[0] . "', \$filter['" . $filterParts[0] . "']);",
                'whereNull' => "\t\t\t\$query->whereNull('" . $filterParts[0] . "');",
                'whereNotNull' => "\t\t\t\$query->whereNotNull('" . $filterParts[0] . "');",
                default => "\t\t\t\$query->where('" . $filterParts[0] . "', \$filter['" . $filterParts[0] . "']);",
            };

            $queryFilter .= "\n\t\t}\n";
        }

        return $queryFilter;
    }

    protected function prepareFillable($fields): string
    {
        if (!$fields) {
            return '';
        }

        $fieldsArray = explode(';', $fields);
        $fillable = [];

        foreach ($fieldsArray as $field) {
            $name = explode('=', $field)[0];
            $fillable[] = "'$name'";
        }

        return implode(', ', $fillable);
    }

    protected function prepareChangePayload($controllerContent, $name, $isUpdated = false): array
    {
        $name = Str::plural(lcfirst(Str::camel($name)));
        $pattern = '/\$payload\s*=\s*\$request->only\(\[\s*\'\w+\'(?:,\s*\'\w+\')*\s*]\);/';

        if (preg_match_all($pattern, $controllerContent, $matches)) {
            if ($isUpdated) {
                $originalFieldArray = $matches[0][1];
            } else {
                $originalFieldArray = $matches[0][0];
            }

            $newFields = substr($originalFieldArray, 0, -3);
            $newFields .= ", '$name'";
            if ($isUpdated) {
                $newFields .= ", '{$name}_deleted'";
            }

            $newFields .= "]);";

            return [
                'original' => $originalFieldArray,
                'new' => $newFields
            ];
        }
        return [
            'original' => '',
            'new' => ''
        ];
    }

    private function prepareFilters($filters): string
    {
        if (!$filters) {
            return '';
        }

        $filtersArray = explode(';', $filters);
        $queryFilter = "\$filter = [";

        foreach ($filtersArray as $filter) {
            $filterParts = explode('=', $filter);
            $key = $filterParts[0];
            $queryFilter .= "\n\t\t\t'$key' => \$request->$key ?? '',";
        }

        $queryFilter .= "\n\t\t];\n";
        return $queryFilter;
    }

    protected function prepareChangeFilters($controllerContent, $filters): array
    {
        if (!$filters) {
            return [
                'original' => '',
                'new' => ''
            ];
        }

        $filtersArray = explode(';', $filters);
        $pattern = '/(\$filter\s*=\s*\[.*?];)/s';

        if (preg_match($pattern, $controllerContent, $matches)) {
            $originalFilterArray = $matches[1];

            $newFilters = substr($originalFilterArray, 0, -2);
            foreach ($filtersArray as $filter) {
                $newFilters .= "\t\t\t'$filter' => \$request->$filter ?? '',";
            }

            $newFilters .= "\n\t\t];\n";
            return [
                'original' => $originalFilterArray,
                'new' => $newFilters
            ];
        }
        return [
            'original' => '',
            'new' => ''
        ];
    }

    protected function prepareUploadPayload($rules): array
    {
        if (!$rules) {
            return [
                'constUpload' => '',
                'uploadFunction' => '',
                'initiateUpload' => ''
            ];
        }

        $constUpload = '';
        $uploadFunction = '';
        $initiateUpload = '';
        $rules = explode(';', $rules);

        foreach ($rules as $createRule) {
            $fieldDetails = explode('=', $createRule);
            $fieldName = $fieldDetails[0];

            $data = explode('|', $fieldDetails[1]);
            foreach ($data as $d) {
                if ($d === 'file') {
                    $name = str_replace(' ', '_', strtoupper($fieldName));
                    $nameFile = 'file-' . str_replace(' ', '-', strtolower($fieldName));
                    $varName = $name . '_FILE_DIRECTORY';
                    if ($uploadFunction === '') {
                        $uploadFunction .= "\n\tprivate function uploadGetPayload(array \$payload): array\n\t{\n";
                        $constUpload = "const $varName = '$nameFile';";
                        $initiateUpload = "\n\t\t\t\$payload = \$this->uploadGetPayload(\$payload);";
                    }
                    $uploadFunction .= "\t\tif (!empty(\$payload['$fieldName'])) {";
                    $uploadFunction .= "\n\t\t\t\$fileName = \$this->generateFileName(\$payload['$fieldName'], '$name' . '_' . date('Ymdhis'));";
                    $uploadFunction .= "\n\t\t\t\$photo = \$payload['$fieldName']->storeAs(self::$varName, \$fileName, 'public');";
                    $uploadFunction .= "\n\t\t\t\$payload['$fieldName'] = \$photo;";
                    $uploadFunction .= "\n\t\t} else {";
                    $uploadFunction .= "\n\t\t\tunset(\$payload['$fieldName']);";
                    $uploadFunction .= "\n\t\t}\n";
                }
            }
        }

        if ($uploadFunction !== '') {
            $uploadFunction .= "\n\t\treturn \$payload;\n\t}";
        }

        return [
            'constUpload' => $constUpload,
            'uploadFunction' => $uploadFunction,
            'initiateUpload' => $initiateUpload,
        ];
    }

    protected function prepareImportModelChildren($name, $helperContent, $parentModel): array
    {
        $name = ucfirst($name);
        $originalImport = "use App\Models\\$parentModel;";
        $importModel = '';

        if (str_contains($helperContent, $originalImport)) {
            $importModel = $originalImport . "\nuse App\Models\\{$name}Model;";
        }

        return [
            'originalImport' => $originalImport,
            'importModel' => $importModel
        ];
    }

    protected function prepareVarChildren($name, $helperContent, $capitalizedName): array
    {
        $originalVar = '';
        $addVar = '';
        if (preg_match('/private\s+\$[^;]+;/', $helperContent, $matches)) {
            $originalVar = $matches[0];
            $addVar = $originalVar . "\n\tprivate \${$name}Model;";
        }

        $originalInitiate = '';
        $addInitiate = '';
        if (preg_match('/\$this->(\w+)\s*=\s*new\s+(\w+)\(\s*\);/', $helperContent, $matches)) {
            $originalInitiate = $matches[0];
            $addInitiate = $originalInitiate . "\n\t\t\$this->{$name}Model = new {$capitalizedName}Model();";
        }

        return [
            'originalVar' => $originalVar,
            'addVar' => $addVar,
            'originalInitiate' => $originalInitiate,
            'addInitiate' => $addInitiate
        ];
    }

    protected function addMethodCrudChildren($name, $helperContent, $parentModel, $relations): string
    {
        $parentName = str_replace('Model', '', $parentModel);
        $nameMethod = ucfirst($name);
        $payloadName = Str::snake(Str::plural($name));
        $relations = explode(';', $relations);
        $foreignKey = '';
        foreach ($relations as $relation) {
            $relationParts = explode(',', $relation);
            if ($relationParts[2] === $parentModel) {
                $foreignKey = $relationParts[3];
                break;
            }
        }
        $parentName = lcfirst($parentName);
        $lastPosition = strrpos($helperContent, "}");

        $crud = "\n\tprivate function delete$nameMethod(array \$$payloadName): void\n" .
            "\t{\n" .
            "\t\tif (empty(\$$payloadName)) {\n" .
            "\t\t\treturn;\n" .
            "\t\t}\n\n" .
            "\t\tforeach (\$$payloadName as \$val) {\n" .
            "\t\t\t\$this->{$name}Model->drop(\$val['id']);\n" .
            "\t\t}\n" .
            "\t}\n\n" .
            "\tprivate function insertUpdate$nameMethod(array \$$payloadName, string \${$parentName}Id): void\n" .
            "\t{\n" .
            "\t\tif (empty(\$$payloadName)) {\n" .
            "\t\t\treturn;\n" .
            "\t\t}\n\n" .
            "\t\tforeach (\$$payloadName as \$val) {\n" .
            "\t\t\tif (isset(\$val['is_added']) && \$val['is_added']) {\n" .
            "\t\t\t\t\$val['$foreignKey'] = \${$parentName}Id;\n" .
            "\t\t\t\t\$this->{$name}Model->store(\$val);\n" .
            "\t\t\t}\n\n" .
            "\t\t\tif (isset(\$val['is_updated']) && \$val['is_updated']) {\n" .
            "\t\t\t\t\$this->{$name}Model->edit(\$val, \$val['id']);\n" .
            "\t\t\t}\n" .
            "\t\t}\n" .
            "\t}\n";

        if ($lastPosition !== false) {
            $helperContent = substr_replace($helperContent, $crud, $lastPosition, 0);
        }

        return $helperContent;
    }

    protected function callMethodChildren($name, $helperContent): array
    {
        $nameMethod = ucfirst($name);
        $payloadName = Str::snake(Str::plural($name));

        $originalCreate = '';
        $addCallMethodCreate = '';
        $patternCreate = '/\$(\w+)\s*=\s*\$this->(\w+)->store\(\$payload\);/';
        if (preg_match($patternCreate, $helperContent, $matches)) {
            $originalCreate = $matches[0];
            $varName = $matches[1];
            $addCallMethodCreate = $originalCreate . "\n\t\t\t\$this->insertUpdate$nameMethod(\$payload['$payloadName'] ?? [], \${$varName}->id);";
        }

        $originalUpdate = '';
        $addCallMethodUpdate = '';
        $patternUpdate = '/\$this->\w+->edit\(\$payload, \$id\);/';
        if (preg_match($patternUpdate, $helperContent, $matches)) {
            $originalUpdate = $matches[0];
            $addCallMethodUpdate = $originalUpdate . "\n\t\t\t\$this->insertUpdate$nameMethod(\$payload['$payloadName'] ?? [], \$id);";
            $addCallMethodUpdate .= "\n\t\t\t\$this->delete$nameMethod(\$payload['{$payloadName}_deleted'] ?? []);";
        }

        return [
            'originalCreate' => $originalCreate,
            'addCallMethodCreate' => $addCallMethodCreate,
            'originalUpdate' => $originalUpdate,
            'addCallMethodUpdate' => $addCallMethodUpdate
        ];
    }

    protected function prepareValidationRules($rules): string
    {
        if (!$rules) {
            return '';
        }

        $validationRules = '';
        $rules = explode(';', $rules);

        foreach ($rules as $rule) {
            $fieldDetails = explode('=', $rule);
            $fieldName = $fieldDetails[0];
            $validationRules .= ($validationRules ? "\n\t\t\t" : '') . "'$fieldName' => '$fieldDetails[1]',";
        }

        return $validationRules;
    }

    protected function prepareChangeValidationRules($name, $requestContent, $rules, $isUpdated = false): array
    {
        if (!$rules) {
            return [
                'original' => '',
                'new' => ''
            ];
        }

        $pattern = $isUpdated
            ? '/(private function updateRules\(\)\s*:\s*array\s*{\s*return\s*\[\s*)(.*?)(\s*];\s*})/s'
            : '/(private function createRules\(\)\s*:\s*array\s*{\s*return\s*\[\s*)(.*?)(\s*];\s*})/s';

        if (preg_match($pattern, $requestContent, $matches)) {
            $originalResource = $matches[0];

            $resourceArray = array_map(function ($rule) use ($name) {
                $parts = explode('=', $rule);
                $attribute = trim($parts[0]);
                $rule = trim($parts[1]);
                return "\n\t\t\t'$name.*.$attribute' => '$rule',";
            }, explode(';', $rules));

            $newResource = $matches[1] . $matches[2] . implode('', $resourceArray) . $matches[3];

            return [
                'original' => $originalResource,
                'new' => $newResource
            ];
        }
        return [
            'original' => '',
            'new' => ''
        ];
    }


    protected function prepareBase64($createRules, $updateRules): array
    {
        if (!$createRules && !$updateRules) {
            return [
                'base64' => '',
                'base64Import' => '',
                'useBase64' => ''
            ];
        }

        $base64 = '';
        $base64Import = '';
        $useBase64 = '';
        $base64Exists = [];
        $createRules = explode(';', $createRules);

        foreach ($createRules as $createRule) {
            $fieldDetails = explode('=', $createRule);
            $fieldName = $fieldDetails[0];

            $data = explode('|', $fieldDetails[1]);
            foreach ($data as $d) {
                if ($d === 'file') {
                    if ($base64 === '') {
                        $base64 .= "protected function base64FileKeys(): array\n\t{\n\t\treturn [\n";
                        $base64Import = "use ProtoneMedia\LaravelMixins\Request\ConvertsBase64ToFiles;";
                        $useBase64 = "use ConvertsBase64ToFiles;\n";
                    }
                    $base64 .= "\t\t\t'" . $fieldName . "' => '" . $fieldName . ".jpg',\n";
                    $base64Exists[] = $fieldName;
                }
            }
        }

        $updateRules = explode(';', $updateRules);
        foreach ($updateRules as $updateRule) {
            $fieldDetails = explode('=', $updateRule);
            $fieldName = $fieldDetails[0];

            $data = explode('|', $fieldDetails[1]);
            foreach ($data as $d) {
                if ($d === 'file') {
                    if ($base64 === '') {
                        $base64 .= "protected function base64FileKeys(): array\n{\n\t\treturn [\n";
                    }
                    if (!in_array($fieldName, $base64Exists)) {
                        $base64 .= "\t\t\t'" . $fieldName . "' => '" . $fieldName . ".jpg',\n";
                        $base64Exists[] = $fieldName;
                    }
                }
            }
        }
        if ($base64 !== '') {
            $base64 .= "\t\t];\n\t}";
        }

        return [
            'base64' => $base64,
            'base64Import' => $base64Import,
            'useBase64' => $useBase64
        ];
    }

    protected function prepareAttributes($name): array
    {
        try {
            $name .= 'Model';
            $model = new ReflectionClass("App\\Models\\$name");
            $fillableAttributes = $model->getDefaultProperties()['fillable'];
        } catch (ReflectionException) {
            return [];
        }

        $attributes = '';
        $resourceImports = '';
        foreach ($fillableAttributes as $attribute) {
            $attributes .= ($attributes ? "\n\t\t\t" : '') . "'$attribute' => \$this->$attribute,";
        }

        $filePath = app_path('Models/' . $name . '.php');
        $relatedModelContent = file_get_contents($filePath);
        $relations = $this->parseRelations($relatedModelContent);

        if ($relations) {
            foreach ($relations as $relation) {
                $resourceName = ucfirst($relation['method']);

                $filePathParent = app_path('Models/' . $relation['class'] . '.php');
                $parentContent = file_get_contents($filePathParent);
                $relationsParent = $this->parseRelations($parentContent);

                if ($relationsParent) {
                    $attributesParent = '';
                    foreach ($relationsParent as $relationParent) {
                        if ($relationParent['class'] == $name) {
                            $resourceParentName = Str::singular(ucfirst($relationParent['method']));
                            $relationParentResource = "App\\Http\\Resources\\{$resourceParentName}Resource";
                            $attributesParent .= ($attributesParent ? "\n\t\t\t" : '') . $this->getAttributeRelation($relationParent, $resourceParentName);
                            $parentResourcePath = app_path("Http/Resources/{$resourceName}Resource.php");
                            $parentResourceContent = file_get_contents($parentResourcePath);

                            if (!str_contains($parentResourceContent, $attributesParent)) {
                                $lastPosition = strrpos($parentResourceContent, "];");

                                if ($lastPosition !== false) {
                                    $newResourceContent = substr_replace($parentResourceContent, "\t$attributesParent\n\t\t", $lastPosition, 0);
                                    file_put_contents($parentResourcePath, $newResourceContent);
                                    $parentResourceContent = file_get_contents($parentResourcePath);
                                }
                            }

                            if (!str_contains($parentResourceContent, $relationParentResource)) {
                                $search = "use Illuminate\Http\Resources\Json\JsonResource;";
                                $import = "use $relationParentResource;\n$search";
                                $newResourceContent = str_replace($search, $import, $parentResourceContent);
                                file_put_contents($parentResourcePath, $newResourceContent);
                            }
                        }
                    }
                }
            }
        }

        return [
            'attributes' => $attributes,
            'resourceImports' => $resourceImports
        ];
    }

    protected function parseRelations($modelContent): array
    {
        $relations = [];
        $pattern = '/public\s+function\s+(\w+)\s*\(\)\s*\{\s*return\s+\$this->(belongsTo|hasOne|hasMany|belongsToMany|morphTo|morphMany|morphToMany|morphOne)\s*\(([^,]+)::class.*\);\s*}/m';

        if (preg_match_all($pattern, $modelContent, $matches, PREG_SET_ORDER)) {
            foreach ($matches as $match) {
                $relations[] = [
                    'method' => $match[1],
                    'type' => $match[2],
                    'class' => trim($match[3])
                ];
            }
        }

        return $relations;
    }

    protected function getAttributeRelation($relation, $resourceName): string
    {
        if (in_array($relation['type'], ['hasMany', 'belongsToMany', 'morphMany', 'morphToMany'])) {
            return "'{$relation['method']}' => {$resourceName}Resource::collection(\$this->{$relation['method']}),";
        } else {
            return "'{$relation['method']}' => new {$resourceName}Resource(\$this->{$relation['method']}),";
        }
    }

    protected function appendRoutes($name): void
    {
        $pluralName = Str::plural($name);
        $controllerName = ucfirst($name) . 'Controller';
        $snakePluralName = Str::snake($pluralName);

        $routes = "\n\tRoute::get('/$snakePluralName', [$controllerName::class, 'index']);"
            . "\n\tRoute::get('/$snakePluralName/{id}', [$controllerName::class, 'show']);"
            . "\n\tRoute::post('/$snakePluralName', [$controllerName::class, 'store']);"
            . "\n\tRoute::put('/$snakePluralName/{id}', [$controllerName::class, 'update']);"
            . "\n\tRoute::delete('/$snakePluralName/{id}', [$controllerName::class, 'destroy']);\n";

        $routePath = base_path('routes/api.php');
        $currentRoutes = file_get_contents($routePath);
        $prefix = "Route::prefix('v1')->group(function () {";
        $newRoutes = str_replace($prefix, $prefix . $routes, $currentRoutes);
        $useStatement = "use App\Http\Controllers\Api\\$controllerName;";
        $newRoutes = preg_replace('/<\?php\n/', "<?php\n\n$useStatement", $newRoutes, 1);

        file_put_contents($routePath, $newRoutes);
    }

    protected function getStub($type): false|string
    {
        return file_get_contents(resource_path("stubs/$type.stub"));
    }
}
