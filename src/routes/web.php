<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Web\GeneratorController;

Route::get('/generate-crud-tool', [GeneratorController::class, 'index']);
Route::post('/generate-crud-tool', [GeneratorController::class, 'generate'])->name('crud.generate.post');
