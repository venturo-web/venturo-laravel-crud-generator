<?php

namespace Venturo\CrudGenerator;

use Illuminate\Support\ServiceProvider;
use Venturo\CrudGenerator\Commands\CrudGeneratorCommand;

class CrudGeneratorServiceProvider extends ServiceProvider
{
    public function boot()
    {
        // Publish stub, view, dll.
        $this->publishes([
            __DIR__.'/resources/stubs' => resource_path('stubs'),
            __DIR__.'/Controller/GeneratorController.php' => app_path('Http/Controllers/Web/GeneratorController.php'),
            __DIR__.'/resources/views/crud-generator.blade.php' => resource_path('views/crud-generator.blade.php'),
        ], 'laravel-assets');
    
        $this->loadRoutesFrom(__DIR__.'/routes/web.php');    
    }

    public function register()
    {
        $this->commands([
            CrudGeneratorCommand::class,
        ]);
    }
}