## Laravel CRUD Generator
==========================

Package ini digunakan untuk menghasilkan operasi CRUD (Create, Read, Update, Delete) pada aplikasi Laravel.

### Instalasi

Untuk menginstal package ini, jalankan perintah berikut di terminal Anda:

```
composer require venturo/laravel-crud-generator:dev-main
```

### Publikasi

Setelah instalasi, publikasikan aset package dengan perintah berikut:

```
php artisan vendor:publish --provider="Venturo\CrudGenerator\CrudGeneratorServiceProvider"
```

### Penggunaan

Untuk menghasilkan operasi CRUD untuk sebuah model, jalankan perintah berikut:
```
php artisan make:crud {name} {--fields=} {--filters=} {--create-rule=} {--update-rule=} {--relations=}
```
contoh :
```
php artisan make:crud Book --fields="m_category_id:string;nullable,penerbit:string" --filters="judul:whereIn" --create-rule="judul=required|max:100;penerbit=in:test,test2" --update-rule="penerbit=in:test,test2"
```
### Opsi

#### `name`

Nama dari model untuk yang akan dibuat operasi CRUD.

#### `--fields`

Daftar kolom untuk migrasi, dipisahkan dengan koma. Setiap kolom harus dalam format `nama:tipe`, di mana `nama` adalah nama kolom dan `tipe` adalah tipe data kolom (contoh: `string`, `integer`, `datetime`, dll.). Anda juga dapat menentukan atribut tambahan untuk setiap kolom seperti `nullable`, `unique`, atau `default:value`.

Contoh: `--fields="name:string,email:string|unique,password:string"`

#### `--filters`

Daftar filter untuk model, dipisahkan dengan koma. Setiap filter harus dalam format `nama:tipe`, di mana `nama` adalah nama filter dan `tipe` adalah jenis filter (contoh: `like`, `whereNot`, `whereIn`, dll.).

Contoh: `--filters="name:like,email:whereNot"`

#### `--create-rule`

Daftar aturan validasi untuk membuat instansi baru dari model, dipisahkan dengan koma. Setiap aturan harus dalam format `field:rule`, di mana `field` adalah nama field dan `rule` adalah aturan validasi (contoh: `required`, `email`, `min:8`, dll.).

Contoh: `--create-rule="name|required,email|required|email"`

#### `--update-rule`

Daftar aturan validasi untuk memperbarui instansi yang ada dari model, dipisahkan dengan koma. Setiap aturan harus dalam format `field:rule`, di mana `field` adalah nama field dan `rule` adalah aturan validasi (contoh: `required`, `email`, `min:8`, dll.).

Contoh: `--update-rule="name|required,email|required|email"`

#### `--relations`

Daftar hubungan untuk model, dipisahkan dengan koma. Setiap hubungan harus dalam format `jenisRelasi:kelas`, di mana `jenisRelasi` adalah jenis relasi (contoh: `hasOne`, `hasMany`, `belongsTo`, dll.) dan `kelas` adalah kelas model terkait.

Contoh: `--relations="hasOne:User,hasMany:Posts"`

### Berkas yang Dibuat

Package ini akan menghasilkan berkas-berkas berikut:

- `app/Models/{NamaModel}.php`: Kelas model.
- `app/Http/Controllers/Api/{NamaModel}Controller.php`: Kelas controller API.
- `app/Http/Requests/{NamaModel}Request.php`: Kelas permintaan untuk membuat dan memperbarui instansi model.
- `app/Http/Resources/{NamaModel}Resource.php`: Kelas sumber daya untuk mentransformasi model menjadi respons JSON.
- `app/Http/Resources/{NamaModel}Collection.php`: Kelas koleksi untuk mentransformasi koleksi model menjadi respons JSON.
- `database/migrations/{timestamp}_create_{nama_tabel}_table.php`: Berkas migrasi untuk membuat tabel.
- `app/Helpers/{NamaModel}Helper.php`: Kelas helper untuk model.

### Lisensi

Package ini dilisensikan di bawah Lisensi MIT. Lihat berkas [LICENSE](LICENSE) untuk detail lebih lanjut.

### Kontribusi

Kontribusi sangat diterima! Silakan submit pull request ke branch [dev-main](https://github.com/venturo/laravel-crud-generator/tree/dev-main).

### Dukungan

Jika Anda memiliki pertanyaan atau masalah, silakan buka isu di [tracker isu GitHub](https://github.com/venturo/laravel-crud-generator/issues).
